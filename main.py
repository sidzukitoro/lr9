def power_2(n):
    if n == 2:
        return True
    if n % 2 == 0:
        return power_2(n / 2)
    else:
        return False
