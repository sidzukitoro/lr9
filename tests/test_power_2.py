from main import power_2


def test_1():
    assert power_2(12) is False, "Не является точной степенью 2"

def test_2():
    assert power_2(16) is True, "Является точной степенью 2"

